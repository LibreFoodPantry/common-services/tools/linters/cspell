# cspell

[CSpell](http://cspell.org/) command line spell checker for code and other
documents.

## Using in the Pipeline

To enable cspell in your project's pipeline add
`cspell` to the `ENABLE_JOBS`
variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  ENABLE_JOBS: "cspell"
```

The `ENABLE_JOBS` variable is a comma-separated string value, e.g.
`ENABLE_JOBS="job1,job2,job3"`

## Using in Visual Studio Code

Install the [Code Spell Checker extension](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
for Visual Studio Code.

## Using Locally in Your Development Environment

CSpell can be run locally in a linux-based bash shell or in a linting shell
script with this Docker command:

```bash
docker run -v "${PWD}:/workdir" \
    ghcr.io/streetsidesoftware/cspell:latest --no-progress \
    "**"
```

## Configuration

Each project should have a `.cspell.json` file which is used for
[CSpell configuration](https://cspell.org/configuration/) including:

- Custom dictionary words for the the specific project
- File or paths to ignore for spell checking

This file is used by the pipeline tool, the Visual Studio Code extension,
and the locally run Docker image.

This file can be manually edited. Adding words using the Visual Studio Code
extension also adds them to this file.

This project has a `.cspell.json` file that can be used as an example.

## Licensing

- Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
- Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
- Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
